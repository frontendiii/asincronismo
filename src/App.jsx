import React, { useState, useEffect } from 'react'
import axios from 'axios'
import './App.css'
import Comentario from './components/Comentario'
import Formulario from './components/Formulario'

function App() {
  const [comentarios, setComentarios] = useState([]);

    useEffect(() => {
        const obtenerComentarios = async () => {
            try {
                const respuesta = await axios.get('https://jsonplaceholder.typicode.com/comments');
                setComentarios(respuesta.data);
            } catch (error) {
                console.log("Error al obtener los comentarios: ", error);
            }
        };
        obtenerComentarios();
    }, []);

    const addComentarios = (comentario) => {
        setComentarios([
            ...comentarios,
            comentario
        ]);
    }

  return (
    <React.Fragment>
      <Comentario comentarios={comentarios}/>
      <br />
      <Formulario addComentarios={addComentarios}/>
    </React.Fragment>
  )
}

export default App
