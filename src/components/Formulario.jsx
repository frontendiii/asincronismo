import React, { useState } from 'react'
import axios from 'axios'

const Formulario = ({ addComentarios }) => {
    const [form, setForm] = useState({
        postId: '',
        id: '',
        name: '',
        email: '',
        body: ''
    });

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const respuesta = await axios.post('https://jsonplaceholder.typicode.com/comments', form);
            addComentarios(respuesta.data);
            console.log(respuesta.data);
        } catch (error) {
            console.log("Error al enviar el comentario: ", error.message);
        }
    };

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    };

  return (
    <div>
      <h2>Agregar comentario: </h2>
      <form onSubmit={handleSubmit}>
        <input
            type="text"
            name="postId"
            placeholder="postId"
            onChange={handleChange}
        />
        <input
            type="text"
            name="id"
            placeholder="id"
            onChange={handleChange}
        />
        <input
            type="text"
            name="name"
            placeholder="name"
            onChange={handleChange}
        />
        <input
            type="text"
            name="email"
            placeholder="email"
            onChange={handleChange}
        />
        <input
            type="text"
            name="body"
            placeholder="body"
            onChange={handleChange}
        />
        <button type="submit">Enviar</button>
      </form>
    </div>
  )
}

export default Formulario
