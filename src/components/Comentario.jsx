import React from 'react'

const Comentario = ({ comentarios }) => {
  return (
    <div>
      <h2>Listado de comentarios: </h2>
      <ul>
        {comentarios.map((comentario) => (
            <li key={comentario.id}>{comentario.body}</li>
        ))}
      </ul>
    </div>
  )
}

export default Comentario
